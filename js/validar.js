/*--------------------------------------*
* Universidade de Sao Paulo             *
* SCC0961 - Web e Mobile Development    *
*---------------------------------------*
* Gustavo Carvalho Araujo (13735630)    *
*---------------------------------------*/

//criando os objetos dos elementos de texto do form

var nome = document.querySelector("#inputName");
var nomeHelp = document.querySelector("#inputNameHelp");
var ano = document.querySelector("#inputYear");
var anoHelp = document.querySelector("#inputYearHelp");
var email = document.querySelector("#inputEmail");
var emailHelp = document.querySelector("#inputEmailHelp");
var senha = document.querySelector("#inputPassword");
var senhaHelp = document.querySelector("#inputPasswordHelp");
var senhaResultado = document.querySelector("#inputSenhaResult");
var senhaBarra = document.querySelector('#passStrengthMeter')
var resultado = document.querySelector('#inputResult')

// Define os tamanhos máximos e mínimos da senha

let tamMin = 6;
let tamMax = 20;

nome.addEventListener('focusout', validarNome);
email.addEventListener('focusout', validarEmail);

document.querySelector('#singleForm').addEventListener('submit', function(event) {
    event.preventDefault();
    validarResultado();
});

function validarNome(e){ 
    // Define o formato válido para o nome, aceitando nomes simples e compostos
    const regexNome = /^[A-Z][a-z]+(?: [A-Z][a-z]+)*$/;
    
    console.log(e); //impressão em console do objeto evento e
    console.log(e.target.value); //impressão em console do valor do objeto 'nome' que originou o evento   

    // Verifica se o nome possui mais que 6 caracteres
    if(e.target.value.trim().length < 6) {
        // Reporta erro e pede para que o nome tenha mais que 6 caracteres
        nomeHelp.textContent = "O nome deve ter mais que 6 caracteres.";
        nomeHelp.style.color = "yellow";
    }
    else if(e.target.value.trim().match(regexNome)==null){
        //muda o conteúdo e o estilo do objeto nomeHelp que referencia o elemento html com id=inputNameHelp
        nomeHelp.textContent = "Formato de nome inválido"; 
        nomeHelp.style.color="red";
    }
    else{
        nomeHelp.textContent = "Nome válido.";
        nomeHelp.style.color = "green";
    }       
}

/*declarando o evento listener para o campos de texto do form. 
Uma vez o foco seja mudado, será chamada a função validarNome*/

//declaração de função de forma anônima usando uma expressão de função de seta =>

ano.addEventListener('focusout', () => {
    //declaração da expressão regular para definir o formato de um ano válido
    const regexAno = /^[0-9]{4}$/;
    //tirar (trim) espaços em branco antes e depois da string
    const anoTrimado = ano.value.trim();
    console.log(ano.value);

    if(anoTrimado.match(regexAno)==null){
        //muda o conteúdo e o estilo do objeto nomeHelp que referencia o elemento html com id=inputYearHelp
        anoHelp.textContent = "Formato de ano inválido";
        anoHelp.style.color="red";
    }
    else{
        //objeto Date
        var date = new Date();
        //obtem o ano atual
        console.log(date.getFullYear()); 
        
        if( parseInt(anoTrimado) > parseInt(date.getFullYear() - 2) ){
             //muda o conteúdo e o estilo do objeto nomeHelp que referencia o elemento html com id=inputYearHelp
            anoHelp.textContent = `Ano inválido. O ano não pode ser maior que ${date.getFullYear() - 2}.`;
            anoHelp.style.color="red";
        }
        else if( parseInt(anoTrimado) < parseInt(date.getFullYear())-124 ){
             //muda o conteúdo e o estilo do objeto nomeHelp que referencia o elemento html com id=inputYearHelp
            anoHelp.textContent = `Ano inválido. O ano não pode ser menor que ${date.getFullYear()-124}.`;
            anoHelp.style.color="red";
        }
        else{
            anoHelp.textContent="Ano validado.";
            anoHelp.style.color="green";
        }        
        
    }
});

// Verifica se o email segue as especificações
function validarEmail(e) {
    // Recebe o tipo do email
    const regexEmail = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.(com|br|net|org)$/;

    console.log(e); //impressão em console do objeto evento e
    console.log(e.target.value); //impressão em console do valor do objeto 'nome' que originou o evento   

    if(e.target.value.trim().match(regexEmail)==null) {
        emailHelp.textContent = "Seu email apresenta um formato inválido. Exemplo de formato válido: joao@email.com";
        emailHelp.style.color = "red";
    } else {
        emailHelp.textContent = "Email validado.";
        emailHelp.style.color= "green";
    }
}

const caracterEspecial = /[@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?\!]/;
const numeroRegex = /[0-9]/;
const letraRegex = /[a-zA-Z]/;

senha.addEventListener('input', () => {
    const minhaSenha = senha.value;
    // Valida senha
    const senhaValida = validarSenha(minhaSenha);

    // Atualiza a barra de força da senha
    if(senhaValida) {
        atualizaForcaSenha(minhaSenha);
    }

    // Inicialmente, todos os itens da lista estão em verde
    let caracterValidado = 'green';
    let corValidada = 'green';
    let letraValidada = 'green';

    // Se a senha não for válida, atualiza as cores conforme necessário
   
    // Verifica se há pelo menos um caractere especial na senha
    caracterValidado = caracterEspecial.test(minhaSenha) ? 'green' : '';
    corValidada = numeroRegex.test(minhaSenha) ? 'green' : '';
    letraValidada = letraRegex.test(minhaSenha) ? 'green' : '';

    senhaHelp.innerHTML = `
    <b style="color:black;" > A senha deve ter ao menos uma ocorrência de: </b>
    <ul>
        <li style="color: ${caracterValidado};">Um caractere especial [@, #, %, &, !, +]</li>
        <li style="color: ${corValidada};">Um número</li>
        <li style="color: ${letraValidada};">Uma letra maiúscula [A-Z]</li>
    </ul>
    `;

});

function validarSenha(senha) {
    // Verifica se a senha tem entre 6 e 20 caracteres
    if (senha.length < tamMin || senha.length > tamMax) {
        senhaResultado.textContent = "A senha deve ter entre 6 e 20 caracteres.";
        senhaResultado.style.color = "black";
        return false;
    }

    // Verifica se a senha contém pelo menos um caractere especial, um número e uma letra
    if (!caracterEspecial.test(senha) || !numeroRegex.test(senha) || !letraRegex.test(senha)) {
        senhaResultado.textContent = "A senha deve conter pelo menos um caractere especial, um número e uma letra.";
        senhaResultado.style.color = "black";
        return false;
    }

    // Verifica se a senha contém o nome ou o ano de nascimento do usuário
    if (senha.toLowerCase().includes(nome.value.toLowerCase()) || senha.includes(ano.value)) {
        senhaResultado.textContent = "A senha não pode conter o nome ou o ano de nascimento do usuário.";
        senhaResultado.style.color ="black";
        return false;
    }

    // Se todas as condições forem atendidas, a senha é válida
    senhaResultado.textContent = "";
    return true;
}

function barraInicial() {
    if(senha.length < 8) {
        senhaBarra.value = 5 * senha.length;
    }
}
function atualizaForcaSenha(senha) {

    const letraMaiusculaRegex = /[A-Z]/
    const comprimento = senha.length;

    let qtdCaracterEspecial = 0;
    let qtdNumero = 0;
    let qtdLetraMaiuscula = 0;

    // Loop através de cada caractere da senha para contabilizar as ocorrências
    for (let i = 0; i < senha.length; i++) {
        const caractere = senha[i];
        if (caracterEspecial.test(caractere)) {
            qtdCaracterEspecial++;
        } else if (numeroRegex.test(caractere)) {
            qtdNumero++;
        } else if (letraMaiusculaRegex.test(caractere)) {
            qtdLetraMaiuscula++;
        }
    }

    if(senha.length < 8) {
        senhaBarra.value += senha.length;
    }

    if (comprimento < 8 && qtdCaracterEspecial > 0 && qtdNumero > 0) {
        senhaResultado.textContent = "Senha fraca.";
        senhaResultado.style.color = "red";
        senhaBarra.value = 11;
    } else {
        senhaResultado.textContent = "Não especificado (Ex: Senha com mais de 8 caracteres e sem letras maiúsuclas).";
        senhaResultado.style.color = "black";
    }
    
    if (comprimento >= 8 && qtdCaracterEspecial > 0 && qtdNumero > 0 && qtdLetraMaiuscula > 0) {
        senhaResultado.textContent = "Senha moderada";
        senhaResultado.style.color = "yellow";
        senhaBarra.value = 20;
    } 
    
    if (comprimento >= 12 && qtdCaracterEspecial > 1 && qtdNumero > 1 && qtdLetraMaiuscula > 1) {
        senhaResultado.textContent = "Senha forte";
        senhaResultado.style.color = "green";
        senhaBarra.value = 30;
    }

}

// Função para validar todos os campos do formulário
function validarResultado() {
    const nomeValido = nomeHelp.style.color === "green";
    const anoValido = anoHelp.style.color === "green";
    const emailValido = emailHelp.style.color === "green";
    const senhaValida = senhaResultado.style.color === "black";

    if (nomeValido && anoValido && emailValido && !senhaValida) {
        resultado.textContent = "Parabéns seus dados foram registrados :)";
        resultado.style.color = "green";
    } else {
        resultado.textContent = "Cadastro inválido";
        resultado.style.color = "red";
    }
}
